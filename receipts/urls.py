from django.urls import path
from .views import receipts_list

urlpatterns = [path("receipts/", receipts_list, name="receipts_list")]
