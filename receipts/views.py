from django.shortcuts import render
from .models import Receipt


def receipts_list(request):
    list_of_receipts = Receipt.objects.all()
    context = {
        "receipts": list_of_receipts,
    }
    return render(request, "receipts/receipt_list.html", context)
